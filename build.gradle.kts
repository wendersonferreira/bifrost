import java.time.Instant
import java.text.SimpleDateFormat
import java.util.Date

plugins {
    kotlin("jvm") version "1.3.70"
    id("com.google.cloud.tools.jib") version "2.1.0"
}

val group = "br.com.trustsystems"
val version = "0.1.0-ALPHA.1"

val tag = if (version.endsWith("-SNAPSHOT"))
    version.replace("-SNAPSHOT", "")
        .plus("_").plus(SimpleDateFormat("yyyyMMddHHmmss").format(Date())) else version

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

jib {
    from {
        image = "amazoncorretto:8u242"
    }
    to {
        image = "registry.gitlab.com/wendersonferreira/bifrost"
        tags = setOf(tag)

        auth {
            username = System.getenv("REGISTRY_USER")
            password = System.getenv("REGISTRY_KEY")
        }
    }
    container {
        labels = mapOf(
            "maintainer" to "Trustsystems <wenderson@trustsystems.com.br>",
            "org.opencontainers.image.title" to "bifrost",
            "org.opencontainers.image.description" to "Bifrost is a bridge between apache thrift and other applications",
            "org.opencontainers.image.version" to tag,
            "org.opencontainers.image.authors" to "Wenderson Ferreira de Souza <wenderson@trustsystems.com.br>",
            "org.opencontainers.image.url" to "https://gitlab.com/trustsystems/bifrost",
            "org.opencontainers.image.vendor" to "https://trustsystems.com.br",
            "org.opencontainers.image.licenses" to "MIT"
        )
        creationTime = Instant.now().toString()
        jvmFlags = listOf(
            "-server",
            "-XX:+UseContainerSupport"
        )
        mainClass = "br.com.trustsystems.bifrost.AppKt"
        ports = listOf("7050")
    }
}

tasks {
    wrapper {
        gradleVersion = "6.2.2"
        distributionType = Wrapper.DistributionType.BIN
    }
}
